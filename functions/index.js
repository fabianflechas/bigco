var functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.createContent = functions.database.ref('/Contents/{uid}/{contentKey}')
  .onCreate(function(event){
    const key = event.params.contentKey;
    const uid = event.params.uid;

    folderRef = admin.database().ref("/Folders/"+uid+"/"+ event.data.folder +"/"+ key);
    folderRef.set(Date.now());
  });

exports.deleteContent = functions.database.ref('/Contents/{uid}/{contentKey}')
  .onDelete(function(event){
    const key = event.params.contentKey;
    const uid = event.params.uid;

    admin.database().ref("/Folders/"+uid+"/"+ key).remove();
  });

exports.createProfileData = functions.auth.user().onCreate(function(event) {

  var user = {
    profile : {
      email: event.data.email,
      birthday: "",
      sex: ""
    },
    publicData: {
      photoUrl: event.data.photoURL,
      name : event.data.displayName.split(" ")[0],
      surname : event.data.displayName.split(" ")[1],
      userName: event.data.displayName,
      timestamp: Date.now(),
      wall: {}
    }
  }
  admin.database().ref("/Users/" + event.data.uid).set(user);

  createMyCoffer(event.data.uid);

  return;
});

function createMyCoffer(uid){
  var myCoffer = {
    name: "my coffer",
    timestamp: Date.now(),
    color: "#fff",
    password: ""
  };
  admin.database().ref("/Folders/" + uid).push(myCoffer);
  return;
}

/*
exports.addToDiscover = functions.database.ref("/Users/{uid}/folders/{folderId}/applications/{appId}")
  .onWrite(function (event) {
    const uid = event.params.uid;
    const appId = event.params.appId;

    var url = event.data.val().url;
    baseUrl = getBaseUrl(url);
    alredyAdded(uid, url, appId)
      .then(function (value) {
        if (value) {
          console.log("ya la habia agregado");
        }else{
          console.log("se añadio al descubre");
          addAppToDiscover(url);
        }
      });
    name = getBaseUrl(url);

  });


function alredyAdded(uid, url, appId) {
  console.log("alredy added - url : " + url);
  let newUrl = getNameFromUrl(url);
  result = new Promise(function (resolve) {
    admin.database().ref("/Users/" + uid + "/folders")
      .once("value")
      .then(
        function (folders) {
          folders.forEach(function (folder) {
            folder.child("applications").forEach(
              function(app){
                if(app.key != appId){
                  appUrl = getNameFromUrl(app.child("url").val());
                  if (appUrl == newUrl) {
                    resolve(true);
                  }
                }
              }
            );
          });
          resolve(false);
        }
      )
  });
  return result;
};

function getBaseUrl(url) {
  console.log("getBaseUrl - url: "+ url);
  splited = url.split("/", 3);
  if (splited[0] == "http:" || splited[0] == "https:") {
    return splited[2]
  } else {
    console.log("getBaseUrl - splited[0]: "+ splited[0]);
    return splited[0]
  }
};

function getNameFromUrl(url){
  baseUrl = getBaseUrl(url);
  console.log("getNameFromUrl - baseUrl: " + baseUrl);
  withSubdomains = baseUrl.split(".");
  name = withSubdomains[0] == "www" ? withSubdomains[1] : withSubdomains[0];
  console.log("result: " + name);
  return name;
};

function addAppToDiscover(url) {
  name = getNameFromUrl(url);
  admin.database().ref("Discover/applications/" + name)
    .once("value")
    .then(function (snapshot) {
      var existe = snapshot.exists();
      if (existe) {
        admin.database().ref("Discover/applications/" + name + "/added").set(snapshot.val().added + 1);
      } else {
        application = {
          url: getBaseUrl(url),
          added: 1
        }
        admin.database().ref("Discover/applications/" + name).set(application);
      }
    })
}
*/
