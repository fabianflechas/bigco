import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {AuthGuard} from "./core/guards/auth.guard";

const routes: Routes = [
  {
    path: "",
    loadChildren: "app/user/user.module#UserModule",
    canActivate: [AuthGuard]
  },
  {
    path: "home",
    loadChildren: "app/home/home.module#HomeModule"
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: ""
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
