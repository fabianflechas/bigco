import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameRecorter'
})
export class NameRecorterPipe implements PipeTransform {

  transform(name: String, args?: any): any {
    return name.length > 15 ? name.substring(0,15)+".." : name;
  }

}
