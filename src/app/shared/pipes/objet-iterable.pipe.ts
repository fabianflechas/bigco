import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objetIterable'
})
export class ObjetIterablePipe implements PipeTransform {

  transform(objects: any, serched: string): any {
    let list_objets = []
    if(serched){
      for(let key in objects){
        if(objects[key].name && objects[key].name.toLowerCase().includes(serched.toLowerCase())){
          objects[key].key = key; 
          list_objets.push(objects[key]);
        }
      }
    }else{
      for(let key in objects){
        if(objects[key].name){
          objects[key].key = key; 
          list_objets.push(objects[key]);
        }
      }
    }
    return list_objets;
  }

}
