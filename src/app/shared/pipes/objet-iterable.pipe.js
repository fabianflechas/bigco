var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
var ObjetIterablePipe = (function () {
    function ObjetIterablePipe() {
    }
    ObjetIterablePipe.prototype.transform = function (objects, serched) {
        var list_objets = [];
        if (serched) {
            for (var key in objects) {
                if (objects[key].name && objects[key].name.toLowerCase().includes(serched.toLowerCase())) {
                    objects[key].key = key;
                    list_objets.push(objects[key]);
                }
            }
        }
        else {
            for (var key in objects) {
                if (objects[key].name) {
                    objects[key].key = key;
                    list_objets.push(objects[key]);
                }
            }
        }
        return list_objets;
    };
    ObjetIterablePipe = __decorate([
        Pipe({
            name: 'objetIterable'
        })
    ], ObjetIterablePipe);
    return ObjetIterablePipe;
}());
export { ObjetIterablePipe };
//# sourceMappingURL=objet-iterable.pipe.js.map