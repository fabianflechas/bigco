import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {ObjetIterablePipe} from "./pipes/objet-iterable.pipe";
import {NameRecorterPipe} from "./pipes/name-recorter.pipe";

@NgModule({
  declarations:[
    ObjetIterablePipe,
    NameRecorterPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    ObjetIterablePipe,
    NameRecorterPipe
  ]
})
export class SharedModule { }
