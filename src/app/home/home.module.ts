import { NgModule } from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {LandingComponent} from "./landing/landing.component";

const LANDING_ROUTES = [
  {
    path:"",
    component: LandingComponent
  }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(LANDING_ROUTES)
  ],
  declarations: [
    LandingComponent
  ]
})
export class HomeModule { }
