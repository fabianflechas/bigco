import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})

export class LandingComponent implements OnInit {
  loggedIn : boolean = false;
  constructor(private authService : AuthService) {

   }

  ngOnInit() {
  }

  googleLogin()  {
    this.authService.logInWtihGoogle()
        .then((success) => {
            this.loggedIn = true;
        }).catch(
          (err) => {
            console.log(err.name , err.message); // crear el html para informar el error al usuario
        });
  }

  facebookLogin(){
    this.authService.logInWtihFAcebook()
        .then((success) => {
            this.loggedIn = true;
        }).catch(
          (err) => {
          console.log(err.name , err.message); // crear el html para informar el error al usuario
        });
  }



}


