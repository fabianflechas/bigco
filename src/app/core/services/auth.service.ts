import { Injectable } from '@angular/core';
import {AngularFireAuth} from "angularfire2/auth";
import * as firebase from 'firebase/app';
import {Router} from "@angular/router";
import {Observable} from "rxjs/Rx";

@Injectable()
export class AuthService {

  constructor(private afAuth : AngularFireAuth, private router : Router) {
  }

  //Devuelve si el usuario se encuentra autenticado con firebase
  isAuthenticated() : Observable<boolean>{
    return this.afAuth.authState
      .map(user => user != null)
  }

  getCurrentUser() : Observable<firebase.User> {
    return this.afAuth.authState;
  }

  getCurrentUserId() {
    return this.afAuth.auth.currentUser ? this.afAuth.auth.currentUser.uid : null;
  }

  logInWtihGoogle(){
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => this.router.navigate(["/"]));
  }

  logInWtihFAcebook(){
    return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => this.router.navigate(["/"]));
  }

  logInWithEmail(email : string, password : string ) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(res => this.router.navigate(["/"]));
  }


  logout() {
    this.afAuth.auth.signOut();
    this.router.navigate(['/home']);
  }

}
