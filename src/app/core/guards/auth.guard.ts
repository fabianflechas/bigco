import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthService} from "../services/auth.service";
import {Subject} from "rxjs/Subject";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router : Router, private authService : AuthService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isAuthenticated()
      .take(1)
      .do(allowed => {
        if(!allowed) {
          this.router.navigate(['/home']);
        }
      });
  }
}
