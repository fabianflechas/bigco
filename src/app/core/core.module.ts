import { NgModule } from '@angular/core';
import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./guards/auth.guard";
import {SharedModule} from "../shared/shared.module";

const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCWAu_GQs49puonx-zBX8dvlWqdcgajsJA',
  authDomain: 'thelauncherone.firebaseapp.com',
  databaseURL: 'https://thelauncherone.firebaseio.com',
  projectId: 'thelauncherone',
  storageBucket: 'thelauncherone.appspot.com',
  messagingSenderId: '538267565710'
};

@NgModule({
  imports: [
    SharedModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [AuthService, AuthGuard]
})
export class CoreModule { }
