export class Profile {

  constructor (
    public $key : string,
    public name : string, //aca irian los componentes del color rgba
    public surname : string,
    public photoUrl: string,
    public timestamp : string,
    public userName: string,
    public birthday: string,
    public email:string,
    public sex:string
  ) {}

  static fromJson($key, name, surname, photoUrl, timestamp, userName, birthday, email, sex){
    return new Profile(
      $key,
      name, //aca irian los componentes del color rgba
      surname,
      photoUrl,
      timestamp,
      userName,
      birthday,
      email,
      sex
    );
  }

}
