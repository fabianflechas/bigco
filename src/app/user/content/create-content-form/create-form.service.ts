import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";

import {Cite} from '../entities/cite';
import {Image} from '../entities/image';
import {Link} from '../entities/link';
import {Text} from '../entities/text';
import {YoutubeVideo} from '../entities/youtube-video';

import {Content} from '../entities/content';


//import {Folder} from "../../folder/folder";
//import {Application} from "../application";
//import {SelectedFolderService} from "../../selected-folder.service";

@Injectable()
export class CreateFormService {

  selectedFolder : string;
  public showForm:Subject<{
    location : string,
    option:string,
    type:string,
    content:Content}> = new Subject();

  // la locacion podria ser el wall o una carpeta
  open(location :string, type:string, content?: Content){
    if(content){
      //modifica una publicacion
      this.showForm.next({location: location, option:'update', type: type, content: content});
    }else{
      //crea algo en una locacion especifica (carpeta)
      this.showForm.next({location : location, option:'create',type: type, content: null});
    }
  }
}
