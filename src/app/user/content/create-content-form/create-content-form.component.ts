import { Component, OnInit,Input } from '@angular/core';
import {CreateFormService} from './create-form.service';
import {Cite} from '../entities/cite';
import {Image} from '../entities/image';
import {Link} from '../entities/link';
import {Text} from '../entities/text';
import {YoutubeVideo} from '../entities/youtube-video';

import {Content} from '../entities/content';
import {ContentService} from "../content.service";
import {PublicationService} from "../../discover/publication/publication.service";


@Component({
  selector: 'create-content-form',
  templateUrl: './create-content-form.component.html',
  styleUrls: ['./create-content-form.component.css']
})
export class CreateContentFormComponent implements OnInit {

  private content :Content;
  loading : boolean = false;
  isAnonimus : boolean = false;
  type:string;
  option:string;
  location:string;

  constructor(private formCreatorService: CreateFormService,
              private contentService : ContentService,
              private publicationsService : PublicationService) {

    formCreatorService.showForm.subscribe(data =>{
      this.option = data.option;
      this.location = data.location; //puede ser publication o application
      if(!data.content){
        this.type = data.type;
        this.content = this.createContentType(data.type);
      }else{
        this.type = data.content.type;
      }
      document.getElementById("openAppModal").click();
    });

  }

  createContentType(type: string): Content{
    switch(type){
      case 'youtube':return YoutubeVideo.createDefaultYoutubeVideo();
      case 'cite':return Cite.createDefaultCite();
      case 'text':return Text.createDefaultText();
      case 'image':return Image.createDefaultImage();
    }
  }

  saveContent(){
    switch(this.location){
      case 'publication':{
        this.loading = true;
        let contentKey = this.contentService.createContent(this.content);
        this.publicationsService.createPublication(contentKey, this.isAnonimus)
          .then(scss => {
            this.loading = false;
            this.closeForm();
          });
        break;
      }
      case 'application':{
        Cite.createDefaultCite();
        break;
      }
    }
  }

  setProperty() { this.isAnonimus = !this.isAnonimus }

  closeForm(){
    document.getElementById("close-content-form").click();
  }

  ngOnInit() {
  }

}
