import { Component, OnInit, Input } from '@angular/core';
import {Content} from './entities/content';
import {DomSanitizer} from "@angular/platform-browser";

declare var $;

@Component({
  selector: 'content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  @Input('content') content:Content;
  @Input('isAnonimus') isAnonimus:boolean = false;
  @Input('inPublication') inPublication : boolean = false;

  constructor(public sanitizer: DomSanitizer) {
  }

  ngOnInit() {

    $(document).ready(function(){
      $('.your-class').not('.slick-initialized').slick({
        arrows:false,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        adaptiveHeight: true,
        speed: 300,
        fade: true,
        cssEase: 'linear'
      });
    });
  }
}
