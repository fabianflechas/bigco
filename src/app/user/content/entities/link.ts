import {Content} from './content'

export class Link implements Content {

  public type : string = 'link';
  

  constructor(
    public $key : string,
    public timestamp : number,
    public source : string,
    public contents : Content[]
  ){}

  static fromJson({$key, timestamp, source, contents}) : Content {
    return new Link(
      $key,
      timestamp, 
      source,
      contents
    );
  }

  static fromJsonArray(array : any[]) : Content[] {
    return array.map(this.fromJson);
  }
}
