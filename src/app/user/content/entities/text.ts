import {Content} from './content';


export class Text implements Content{

  
    public type : string = 'text';

  constructor(
    public $key : string,
    public timestamp : number,
    public title : string,
    public source : string
  ){}

  static fromJson({$key, timestamp,title, source}) : Content {
      return new Text(
        $key,
        timestamp, 
        title,
        source
      );
  }

  static fromJsonArray(array : any[]) : Content[] {
    return array.map(this.fromJson);
  }

  static createDefaultText() : Text {
      return new Text("", Date.now(), "esto es una prueba", "yo");
  }
}






