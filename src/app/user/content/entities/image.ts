import {Content} from "./content"; // text se extiende de content

export class Image implements Content {

  
  public type : string= 'image';

  constructor(
    public $key : string,
    public timestamp : number,
    public source : string
  ){}

  static fromJson({$key, timestamp, source}) : Content {
    return new Image(
      $key,
      timestamp,
      source
    );
  }

  static fromJsonArray(array : any[]) : Content[] {
    return array.map(this.fromJson);
  }

  static createDefaultImage() : Image {
      return new Image("", Date.now(), "assets/images/logo/bc.png");
  }
}









