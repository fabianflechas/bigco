import {Content} from './content';

export class Cite implements Content{

  
  public type : string = 'cite'; 

  constructor(
    public $key : string,
    public timestamp : number,
    public source : string,
    public author : string
  ){}

  static fromJson({$key, timestamp, type, source, author}) : Content {
    return new Cite(
      $key,
      timestamp, 
      source,
      author
    );
  }

  static fromJsonArray(array : any[]) : Content[] {
    return array.map(this.fromJson);
  }

  static createDefaultCite() : Cite {
      return new Cite("", Date.now(), "", "");
  }

}
