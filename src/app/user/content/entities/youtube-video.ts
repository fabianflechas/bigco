import {Content} from "./content"; // text se extiende de content

export class YoutubeVideo implements Content {
  
  public type : string = 'youtube';

    
  constructor(
    public $key : string,
    public timestamp : number,
    public source : string
  ){}

  static fromJson({$key, timestamp,source, author}) : Content {
    return new YoutubeVideo(
      $key,
      timestamp,
      source
    );
  }

  static fromJsonArray(array : any[]) : Content[] {
    return array.map(this.fromJson);
  }

   static createDefaultYoutubeVideo() : YoutubeVideo {
      return new YoutubeVideo("", Date.now(), "");
  }
}
