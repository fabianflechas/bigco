import { Component, OnInit, ViewChild, Input } from '@angular/core';
//image recorter
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';

const ICON_IMAGE_UPLOADER = {
  stand: 'fa fa-upload fa-3x white',
  loading: 'fa fa-spin fa-circle-o-notch fa-3x white',
  uploaded: 'fa fa-check fa-3x fa-desapear green',
  over: 'fa fa-upload fa-3x fa-bounce white',
  error: 'fa fa-times fa-3x fa-pulse red'
}



@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent implements OnInit {

  ngOnInit(){}

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;
  edit = false;
  data: any;
  cropperSettings: CropperSettings;
  imageLoaded: boolean = false;
  imageSrc: string = '';
  //style icon
  iconClass = ICON_IMAGE_UPLOADER['stand'];


  width: 100;
  height: 600;

  activeEdit(){
    this.edit = true;
  }

  inactiveEdit(){
    this.edit = false;
  }


  constructor() {
    this.cropperSettings = new CropperSettings();


    this.cropperSettings.noFileInput = true;

    this.cropperSettings.width = 450;
    this.cropperSettings.height = 300;

    this.cropperSettings.croppedWidth = 450;
    this.cropperSettings.croppedHeight = 300;

    this.cropperSettings.canvasWidth = 450;
    this.cropperSettings.canvasHeight = 300;

    this.cropperSettings.minWidth = 45;
    this.cropperSettings.minHeight = 30;

    this.cropperSettings.rounded = false;
    //this.cropperSettings.keepAspect = false;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.data = {};
  }


  handleDragEnter() {
      this.iconClass = ICON_IMAGE_UPLOADER['over'];
  }

  handleDragLeave() {
    if(this.data.image){
      this.iconClass = ICON_IMAGE_UPLOADER['uploaded'];
    }else{
      this.iconClass = ICON_IMAGE_UPLOADER['stand'];
    }
  }

  handleDrop(e) {
    this.iconClass = ICON_IMAGE_UPLOADER['loading'];
      e.preventDefault();
      this.handleInputChange(e);
  }

  handleImageLoad() {
    this.iconClass = ICON_IMAGE_UPLOADER['uploaded'];
    this.imageLoaded = true;
  }


  handleInputChange(e) {
      var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

      var pattern = /image-*/; //tipe of file that whe accept

      if (!file.type.match(pattern)) {
          this.iconClass = ICON_IMAGE_UPLOADER['error'];
          return;
      }
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
  }

  private _handleReaderLoaded(e) {
      var reader = e.target;
      this.imageSrc = reader.result;
      var image:any = new Image();
      image.src = this.imageSrc;
      this.cropper.setImage(image);
      this.iconClass = ICON_IMAGE_UPLOADER['uploaded'];
  }

}
