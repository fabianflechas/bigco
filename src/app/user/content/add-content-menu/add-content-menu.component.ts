import {AfterContentChecked, AfterViewChecked, AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FolderFormService} from "../../dashboard/folder/folder-form/folder-form.service";
import {ApplicationFormService} from "../../dashboard/application/application-form/application-form.service";


import {CreateFormService} from '../create-content-form/create-form.service';

declare var $;

@Component({
  selector: 'app-add-content-menu',
  templateUrl: './add-content-menu.component.html',
  styleUrls: ['./add-content-menu.component.css']
})
export class AddContentMenuComponent implements OnInit, AfterViewInit {

  @Input() mode : string = "dashboard";
  isOn : boolean = false;

  constructor(
    private folderFormService: FolderFormService,
    private appFormService: ApplicationFormService,
    private createFormService: CreateFormService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.reset()
  }

  openAddFolderModal() {
    this.close();
    this.folderFormService.open();
  }

  openAddAppModal() {
    this.close();
    this.appFormService.open();
  }

  openCreateForm(contentType:string){
    this.close();
    this.createFormService.open(this.mode, contentType);
  }

  close(){
    if(this.isOn) this.reset()
  }

  addActions(){
    console.log("sdfsd");
    if(this.isOn) {
      this.reset();
    } else {
      this.setPosition();
    }
  }

  setPosition() {
    this.isOn = true;
    $("#content-menu-button").addClass("rotate");
    $("#dark-div-content").addClass("m-fadeIn");
    $("#dark-div-content").removeClass("m-fadeOut");

    var links = $('#add-items-container div');
    console.log(links.length);
    var radius = (links.length * links.width()) / 2;
    var degree = Math.PI / links.length, angle = degree / 2;

    links.each(function () {
      var x = Math.round(radius * Math.cos(angle));
      var y = Math.round(radius * Math.sin(angle));

      $('#add-items-container div').removeClass("m-fadeOut");
      $(this).addClass("m-fadeIn");
      $(this).css({
        left: x + 'px',
        top: '-'+ y + 'px'
      });
      angle += degree;
    });
  }

  reset() {
    $("#content-menu-button").removeClass("rotate");

    $("#dark-div-content").removeClass("m-fadeIn");
    $("#dark-div-content").addClass("m-fadeOut");

    $('#add-items-container div').removeClass("m-fadeIn");
    $('#add-items-container div').addClass("m-fadeOut");
    $('#add-items-container div').css({
      left: 0 + 'px',
      top: 0 + 'px'
    });

    this.isOn = false;
  }

}
