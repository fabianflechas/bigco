import { Injectable } from '@angular/core';
import {Content} from './entities/content';

import {Cite} from './entities/cite';
import {Image} from './entities/image';
import {Link} from './entities/link';
import {Text} from './entities/text';
import {YoutubeVideo} from './entities/youtube-video';

import {AngularFireDatabase} from "angularfire2/database";
import {AuthService} from "../../core/services/auth.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class ContentService {

  constructor(private db : AngularFireDatabase,
              private authService: AuthService) {}

  public createContent(content: Content) : string{
    delete content.$key;
    return this.db.list("Contents/" + this.authService.getCurrentUserId()).push(content).key;
  }

  public getContent(user : string, content: string) : Observable<Content> {
    return this.db.object("Contents/" + user + "/" + content)
      .map(
        content =>{
          let maped;
          switch (content.type){
            case ('text') : {
              maped = Text.fromJson(content);
              break;
            }
            case ('cite') : {
              maped =  Cite.fromJson(content);
              break;
            }
            case ('link') : {
              maped = Link.fromJson(content);
              break;
            }
            case ('youtube') : {
              maped = YoutubeVideo.fromJson(content);
              break;
            }
            case ('image') : {
              maped = Image.fromJson(content);
              break;
            }
          }
          return maped
        }
      )
  }

}
