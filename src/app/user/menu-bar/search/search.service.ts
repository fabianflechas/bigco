import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";

@Injectable()
export class SearchService {

  public search : Subject<string> = new Subject();

  refreshSearch( search : string) {
    this.search.next(search);
  }
}
