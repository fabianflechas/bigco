import { Component, OnInit } from '@angular/core';
import {SearchService} from "./search.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  openFlag = false;
  icon : string = "fa-search";
  searched = "";

  constructor(private searchService : SearchService) { }

  ngOnChanges() {
    this.searchService.refreshSearch(this.searched);
  }

  ngOnInit() {
  }

  openCloseEvent(){
    this.openFlag = !this.openFlag;
    this.openFlag? this.icon = "fa-search" : this.icon = "fa-times";
    this.searched = "";
  }

}
