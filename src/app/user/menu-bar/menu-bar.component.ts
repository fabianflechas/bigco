import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthService} from "../../core/services/auth.service";

declare var $;

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})

export class MenuBarComponent implements OnInit {

  user;

  constructor(public router:Router, route:ActivatedRoute, private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.getCurrentUser().subscribe(
      userInfo => {
        this.user = userInfo
      }
    );
  }

  logOut(){
    this.authService.logout()
  }
}
