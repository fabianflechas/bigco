import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Folder} from "./folder/folder";

@Injectable()
export class SelectedFolderService {

  folder : Folder = null;
  public selectedfolder:Subject<{folder: Folder}> = new Subject();

  selectFolder(folder: Folder){
    this.selectedfolder.next({folder: folder});
  }

}
