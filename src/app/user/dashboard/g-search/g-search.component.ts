import { Component, OnInit } from '@angular/core';

declare var $;

@Component({
  selector: 'app-g-search',
  templateUrl: './g-search.component.html',
  styleUrls: ['./g-search.component.css']
})
export class GSearchComponent implements OnInit {

   openFlag = false;
   query=null;

  constructor() { }

  ngOnChanges() {
  }

  ngOnInit() {
  }

  openCloseEvent(){
    this.openFlag = !this.openFlag;
    if(this.openFlag){
      $("#gsearch").removeClass("col-lg-2 col-md-3 col-4");
      $("#gsearch").addClass("col-lg-4 col-md-6 col-12");
    }else{
      $("#gsearch").addClass("col-lg-2 col-md-3 col-4");
      $("#gsearch").removeClass("col-lg-4 col-md-6 col-12");
    }
    if(this.query){
      var win = window.open("https://www.google.com.ar/#q="+ this.query, '_blank');
      win.focus();
      this.query = null;
    }
  }

}
