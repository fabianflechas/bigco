import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import {AuthService} from "../../../core/services/auth.service";
import 'rxjs/add/operator/map';
import {Folder} from "../folder/folder";
import {Application} from "./application";

@Injectable()
export class ApplicationsService {

  constructor(private db : AngularFireDatabase,
              private authService: AuthService) { }


  //crea una nueva aplicacion
  public createNewApplication(app: Application, folder: Folder){

    //elimina el campo key vacio para que no tenga conflicto con firebase
    delete app.$key;
    //agrega la aplicacion y guarda la key asignada por firebase
    let appKey = this.db.list("Applications/"+this.authService.getCurrentUserId()).push(app).key;
    //guarda la key en la carpeta indicada junto con el timestamp actual
    this.db.object("Folders/"+this.authService.getCurrentUserId()+"/"+folder.$key+"/applications/"+appKey).set(Date.now())
      .then(
        application => {
          console.log("exito al guardar la aplicacion" + application);
        })
      .catch(err => console.log("se produjo un error al guardar la aplicacion" + err));
  }

  //elimina una alpicacion

  public deleteApplication( app : Application, folder : Folder){

    //elimina el objeto aplicacion
    this.db.object("Applications/"+this.authService.getCurrentUserId()+"/"+app.$key).remove();
    //elimina la referencia a la aplicacion de la carpeta donde se encontraba
    this.db.object("Folders/"+this.authService.getCurrentUserId()+"/"+folder.$key+"/applications/"+app.$key).remove()
      .then(a => console.log("exito al eliminar la aplicacion"))
      .catch(err => console.log("se produjo un error al eliminar la aplicacion"));
  }

  //actuliza una aplicacion

  public updateApplication( updatedApp : Application){

    //elimina la key para evitar conflictos con firebase
    delete updatedApp.$key;
    //actualiza la aplicacion
    this.db.object("Applications/"+this.authService.getCurrentUserId()+"/"+updatedApp.$key).update(updatedApp)
      .then(a => console.log("exito al actualizar la aplicacion"))
      .catch(err => console.log("se produjo un error al actualizar la aplicacion"));
  }

}
