export class Application {

  constructor (
    public $key : string,
    public background : string, //aca irian los componentes del color rgba
    public color: string, //idem
    public icon: string,
    public isfavicon : boolean,
    public name: string,
    public content: string
  ) {}

  static fromJson({$key, background, color, icon, isfavicon, name, content}){
    return new Application(
      $key,
      background,
      color,
      icon,
      isfavicon,
      name,
      content
    );
  }

  static fromJsonArray(array : any[]) : Application[] {
    return array.map(Application.fromJson);
  }

  static createDefaultApplication() : Application {
    return new Application('','#ffffff','#ff0000','arrow-up', false,'new_app','');
  }

}
