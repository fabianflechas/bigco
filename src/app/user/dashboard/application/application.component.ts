import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  @Input() app_details;

  constructor() {
  }

  openUrl(){
    if(this.app_details.name != "") window.open(this.app_details.url, 'blank');
  }

  ngOnInit() {
  }

}
