import {Subject} from 'rxjs/Rx';
import {EventEmitter, Injectable} from "@angular/core";
import {Folder} from "../../folder/folder";
import {Application} from "../application";
import {SelectedFolderService} from "../../selected-folder.service";

@Injectable()
export class MenuAppService{
  private selectedFolder : Folder;
  public show:Subject<{event:MouseEvent,app:Application, folder: Folder}> = new Subject();
  public menuClosed : EventEmitter<null> = new EventEmitter();

  constructor(private selectedFolderService : SelectedFolderService){
    this.selectedFolderService.selectedfolder.subscribe(
      event => this.selectedFolder = event.folder
    );
  }

  public showContext(event: MouseEvent, app: Application){
    this.show.next({
      event: event,
      app: app,
      folder: this.selectedFolder
    });
  }

  public closeMenu(){
    this.menuClosed.emit();
  }
}
