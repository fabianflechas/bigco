import {Directive, HostBinding, HostListener, Input} from "@angular/core";
import {MenuAppService} from "./menu-app.service";
import {Application} from "../application";

declare var $;

@Directive({
  selector:'[app-menu]'
})

export class MenuAppDirective{
  @Input('app-menu') application : Application;
  @Input('app-id') appId : string;

  @HostListener('contextmenu', ['$event']) rightClicked(event:MouseEvent){
    this._contextMenuService.showContext(event, this.application);
    event.preventDefault();
    this.z_index = 100;
    this.color = "#fff";
    let app = $('#' + this.appId);
    let offset = app.offset();
    let appViewportOffsetTop = offset.top - $(document).scrollTop();
    if(appViewportOffsetTop < 100){
      $('body').animate({
        scrollTop: appViewportOffsetTop + 50
      }, 500);
    }
  }
  @HostBinding('style.z-index') z_index = 1;
  @HostBinding('style.color') color = "#000000";

  constructor(private _contextMenuService:MenuAppService){
    _contextMenuService.menuClosed.subscribe(
      close => {
        this.z_index = 1;
        this.color = "#000000";
      }
    )
  }

}
