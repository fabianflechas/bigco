import {Component, OnInit} from '@angular/core';
import {ApplicationFormService} from "../application-form/application-form.service";
import {MenuAppService} from "./menu-app.service";
import {Application} from "../application";
import {Folder} from "../../folder/folder";
import {ApplicationsService} from "../applications.service";

declare var $;

@Component({
  selector: 'app-context-menu',
  templateUrl: './menu-app.component.html',
  styleUrls: ['./menu-app.component.css'],
  host:{
    '(document:click)':'closeMenu()',
    '(window:scroll)':'closeMenu()',
    '(window:resize)':'closeMenu()'
  }
})
export class MenuAppComponent implements OnInit {

  app : Application = null;
  folder : Folder = null;

  constructor(
    private contextMenuService:MenuAppService,
    private appService : ApplicationsService,
    private appFormService : ApplicationFormService){

    this.contextMenuService.show.subscribe(e => {
      this.app = e.app;
      this.folder = e.folder;
      this.openMenu();
    });
  }

  ngOnInit(){
  }

  deleteApp() {
    this.closeMenu();
    this.appService.deleteApplication(this.app, this.folder);
  }

  updateApp() {
    this.closeMenu();
    this.appFormService.open(this.app);
  }

  openMenu() {

    $("#dark-div").addClass("m-fadeIn");
    $("#dark-div").removeClass("m-fadeOut");

    $('#app-menu-container').removeClass("m-fadeOut");
    $('#app-menu-container').addClass("m-fadeIn");
  }

  closeMenu() {

    this.contextMenuService.closeMenu();

    $("#dark-div").removeClass("m-fadeIn");
    $("#dark-div").addClass("m-fadeOut");

    $('#app-menu-container').removeClass("m-fadeIn");
    $('#app-menu-container').addClass("m-fadeOut");


  }

}
