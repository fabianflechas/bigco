import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ApplicationFormService} from "./application-form.service";
import {Application} from "../application";
import {Folder} from "../../folder/folder";
import {ApplicationsService} from "../applications.service";

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css']
})
export class ApplicationFormComponent implements OnInit {

  @Input('application') modelApp : Application = Application.createDefaultApplication();
  @Input('option') option : string = 'create';
  @Input('folder') folder : Folder;
  colorTouched : boolean = false;

  constructor(
    private router: Router,
    private appService : ApplicationsService,
    private appFormService : ApplicationFormService
  ) {
    this.appFormService.showForm.subscribe(e => {
      this.option = e.option;
      this.folder = e.folder;
      switch (this.option){
        case 'create': {
          this.modelApp = Application.createDefaultApplication();
          break
        }
        case 'update': {
          this.modelApp =
            new Application(
              e.application.$key,
              e.application.background,
              e.application.color,
              e.application.icon,
              e.application.isfavicon,
              e.application.name,
              ""
              //e.application.url
            );
          break
        }
      }
      document.getElementById("openAppModal").click();
      this.colorTouched = false;
    });
  }

  onSubmit(form){
    console.log("submitiado: " + this.modelApp);
    this.saveMarker();
    form.resetForm();
    document.getElementById("cancelApp").click();
  }

  ngOnInit() {
  }

  go(){
    this.router.navigate(['/dashboard']);
  }

  appClick(event){
    console.log("tag: " + event.target.tagName);
    if(event.target.tagName == "I"){
      document.getElementById("colorFolder").click();
      this.colorTouched = true;
    }else{
      if(event.target.tagName == "DIV" || event.target.tagName == "NG2-FA"){
        document.getElementById("backgroundFolder").click();
        this.colorTouched = true;
      }
    }
  }

  iconSelected(icon : string[]){
    if(icon[1] == "icon"){
      this.modelApp.icon = icon[0];
      this.modelApp.isfavicon = false;
    }else{
      this.modelApp.icon = icon[0];
      this.modelApp.isfavicon = true;
    }

  }

  public saveMarker(){
    switch (this.option){
      case 'create': {
        this.appService.createNewApplication(this.modelApp, this.folder);
        break
      }
      case 'update': {
        this.appService.updateApplication(this.modelApp);
        break
      }
    }
  }

  /*setSmartName(){
    let specialSubdomains = ["www", "com", "edu", "gob", "org"];
    let smartName = "";
    let splitUrl = this.modelApp.url.split("/",3);
    let nameWithSpecialDomains = splitUrl[0] == "https:" || splitUrl[0] == "http:" ? splitUrl[2] : splitUrl[0];

    for(let subdomain of nameWithSpecialDomains.split('.')){
      if(specialSubdomains.indexOf(subdomain) > -1){
        smartName += subdomain + " ";
      }
    }
    console.log(smartName);

    this.modelApp.name = smartName;
  }*/

}

