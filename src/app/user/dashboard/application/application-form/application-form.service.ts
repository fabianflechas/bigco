import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Folder} from "../../folder/folder";
import {Application} from "../application";
import {SelectedFolderService} from "../../selected-folder.service";

@Injectable()
export class ApplicationFormService {

  selectedFolder : Folder;
  public showForm:Subject<{application:Application, folder : Folder, option:string}> = new Subject();

  constructor(private selectedFService : SelectedFolderService){
    this.selectedFService.selectedfolder
      .subscribe(
        event => {
          this.selectedFolder = event.folder;
        }
      )
  }
  open(application? : Application){
    if(application){
      this.showForm.next({application: application, folder: this.selectedFolder, option:'update'});
    }else{
      this.showForm.next({application: null, folder : this.selectedFolder, option:'create'});
    }
  }
}
