import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconFilter'
})
export class IconFilterPipe implements PipeTransform {

  transform(icons: any, search: string): any[] {
    let result = [];
    if(search){
      for(let icon of icons){
        if(icon.includes(search.toLowerCase())){
          result.push(icon);
        }
      }
      return result;
    }else{
      return icons;
    }
  }

}
