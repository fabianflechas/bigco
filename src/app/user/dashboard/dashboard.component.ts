import { Component, OnInit } from '@angular/core';
import {FoldersService} from "./folder/folders.service";
import {SearchService} from "../menu-bar/search/search.service";
import {SelectedFolderService} from "./selected-folder.service";
import {Folder} from "./folder/folder";
import {Observable} from "rxjs/Observable";
import {Application} from "./application/application";

declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  colapsado : boolean = false;
  applications: Observable<Application[]> = null;
  folders: Observable<Folder[]> = null;
  selectedFolder: Folder;
  searched: string = '';

  constructor(
    private folderService: FoldersService,
    private searchService: SearchService,
    private selectedFService: SelectedFolderService
  ) {

    this.searchService.search.subscribe(
      newSearch => this.searched = newSearch
    );
    this.folders = this.folderService.getAllFolders();
    this.selectedFService.selectedfolder.subscribe(
      event => {
        this.selectedFolder = event.folder;
        this.applications = this.folderService.getApplications(this.selectedFolder);
      }
    );
  }

  scrollLeft(){
    $("#folders").animate({
      scrollLeft: "+=" + -200 + "px"
    });
  }

  scrollRight(){
    $("#folders").animate({
      scrollLeft: "+=" + 200 + "px"
    })
  }

  ngOnInit() {
    $('#folders').on('hide.bs.collapse', function () {
      $("#scrollL").hide();
      $("#scrollR").hide();
    })
    $('#folders').on('show.bs.collapse', function () {
      $("#scrollL").show();
      $("#scrollR").show();
    });

    this.folderService.getAllFolders().subscribe(
      folders => {

        if (this.selectedFolder == null || !(folders.findIndex(folder => folder.$key == this.selectedFolder.$key)>0)) {
          this.selectedFolder = folders[0];
          this.selectedFService.selectFolder(folders[0]);
        }
      }
    );

    $('#folders').on('hide.bs.collapse', () => {
      this.colapsado = true;
      setTimeout(this.showName, 500);
    });

    $('#folders').on('show.bs.collapse', () => {
      this.colapsado = false;
      this.hideName();
    });
  }

  hideName(){
    $('#folderName').css({
      transition: 'none',
      width: '0px',
      height: '0px'
    });
    $('#folderName').addClass('m-fadeOut');
    $('#folderName').removeClass('m-fadeIn');
  }

  showName(){
    $('#folderName').css({
      transition: 'all .5s ease',
      width: 'auto',
      height: 'auto'
    });
    $('#folderName').removeClass('m-fadeOut');
    $('#folderName').addClass('m-fadeIn');
  }
}
