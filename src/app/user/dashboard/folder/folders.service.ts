import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import {AuthService} from "../../../core/services/auth.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {Folder} from "./folder";

@Injectable()
export class FoldersService {

  constructor(private db : AngularFireDatabase, private authService: AuthService) { }

  //obtiene todas las carpetas del usuario actual

  public getAllFolders() : Observable<Folder[]> {
    if(this.authService.isAuthenticated()) {
      return this.db.list('Folders/' + this.authService.getCurrentUserId())
        .map(Folder.fromJsonArray);
    }
    return;
  }

  //crea una nueva carpeta para el usuario actual

  public createNewFolder(newFolder : Folder){
    delete newFolder.$key; //elimina el campo key vacio para que no tenga conflicto con firebase
    let folders = this.db.list("/Folders/"+this.authService.getCurrentUserId());
    //newFolder.applications.push('');
    folders.push(newFolder)
      .then(a => console.log("exito al guardar la carpeta" + a))
      .catch(err => console.log("se produjo un error al guardar la carpeta" + err));
  }

  //elimina la carpeta con la KEY especificada

  public deleteFolder( key : string ){
    let folder = this.db.object("/Folders/"+this.authService.getCurrentUserId()+"/"+key);
    folder.remove()
      .then(a => console.log("exito al eliminar la carpeta" + a))
      .catch(err => console.log("se produjo un error al eliminar la carpeta" + err));
  }

  //actualiza los datod de una carpeta

  public updateFolder( updatedApp : Folder){
    let folder = this.db.object("/Folders/"+this.authService.getCurrentUserId()+"/"+ updatedApp.$key);
    delete updatedApp.$key;
    folder.update(updatedApp)
      .then(a => console.log("exito al actualizar el marcador" + a))
      .catch(err => console.log("se produjo un error al actualizar el marcador" + err));
  }

}
