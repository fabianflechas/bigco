import {Component, Input, OnInit} from '@angular/core';
import {Md5} from "ts-md5/dist/md5";
import {SelectedFolderService} from "../selected-folder.service";

declare var $;

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit {

  @Input() folder_details;
  @Input() isSelected : boolean = false;
  password : string = '';
  error : string = "";

  constructor(private selectedFService : SelectedFolderService) { }

  ngOnInit() {
  }

  showPasswordDialog(){
    console.log("show dialog :" + this.folder_details.password);
    if(this.folder_details.password !== ""){
      $("#"+ this.folder_details.$key).modal("show");
    }else{
      this.selectedFService.selectFolder(this.folder_details);
    }
  }

  closePasswordDialog(){
    $("#"+ this.folder_details.$key).modal("hide");
    this.password = "";
    this.error = "";
  }

  openFolder(){
    if(this.checkPassword()){
      this.closePasswordDialog();
      this.selectedFService.selectFolder(this.folder_details);
    }else{
      this.error = "contraseña incorrecta!";
      this.password = "";
    }
  }

  checkPassword() : boolean{
    if(Md5.hashStr(this.password) === this.folder_details.password || this.folder_details.password === ''){
      return true;
    }else{
      return false;
    }
  }
}
