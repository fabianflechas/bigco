import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FolderFormService} from "./folder-form.service";
import {Md5} from "ts-md5/dist/md5";
import {Folder} from "../folder";
import {FoldersService} from "../folders.service";

@Component({
  selector: 'app-folder-form',
  templateUrl: './folder-form.component.html',
  styleUrls: ['./folder-form.component.css']
})
export class FolderFormComponent implements OnInit {

  @Input('folder') modelFolder: Folder = Folder.createDefaultFolder();
  @Input('option') option: string = 'create';
  password: string = "";
  confirmPassword: string = "";
  colorApp : string = "#ffffff";
  colorTouched: boolean = false;
  passConfirmed : boolean = true;

  constructor(private router: Router,
              private folderService: FoldersService,
              private folderFormService: FolderFormService) {  }

  ngOnInit() {
    this.folderFormService.showForm.subscribe(e => {
      this.option = e.option;
      switch (this.option) {
        case 'create': {
          this.modelFolder = Folder.createDefaultFolder();
          break
        }
        case 'update': {
          this.modelFolder = e.folder;
          break
        }
        default : {

        }
      }
      document.getElementById("openFolderModal").click();
      this.colorTouched = false;
      this.colorApp = this.modelFolder.color;
    });
  }

  openColorInput() {
    document.getElementById("color").click();
    this.colorTouched = true;
  }

  public saveFolder(form) {

    this.modelFolder.name = form.controls["name"].value;
    this.modelFolder.color = this.colorApp;
    if(this.password !== '' && this.password !== null){
      this.modelFolder.password = <string>Md5.hashStr(this.password)
    }else{
      this.modelFolder.password = '';
    }
    switch (this.option) {
      case 'create': {
        this.folderService.createNewFolder(this.modelFolder);
        break
      }
      case 'update': {
        this.folderService.updateFolder(this.modelFolder);
        break
      }
      default : {

      }
    }
    this.password = "";
    this.confirmPassword = "";
    document.getElementById("cancelFolder").click();
  }

}
