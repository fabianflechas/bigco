import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Folder} from "../folder";

@Injectable()
export class FolderFormService {

  public showForm:Subject<{folder:Folder, option:string}> = new Subject();

  open( folder? : Folder){
    if(folder){
      this.showForm.next({folder: folder, option:'update'});
    }else{
      this.showForm.next({folder: null, option:'create'});
    }
  }
}
