export class Folder {

  constructor (
    public $key : string,
    public color : any,
    public name : string,
    public password : string,
    public timestamp: number,
    public contents : string[]
  ) {}

  static fromJson({$key, color, name, password, timestamp, contents}){
    return new Folder(
      $key,
      color,
      name,
      password,
      timestamp,
      contents
    );
  }

  static fromJsonArray(array : any[]) : Folder[] {
    return array.map(Folder.fromJson);
  }

  static createDefaultFolder() : Folder {
    return new Folder("","#6cff61","nueva carpeta",'', Date.now(), []);
  }

}
