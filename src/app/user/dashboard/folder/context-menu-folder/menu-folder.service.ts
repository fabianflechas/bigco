import {Subject} from 'rxjs/Rx';
import {Injectable} from "@angular/core";
import {Folder} from "../folder";

@Injectable()
export class MenuFolderService{
  public show:Subject<{event:MouseEvent, folder: Folder}> = new Subject();

  public showContext(event: MouseEvent, folder: Folder){
    console.log("servicio activado");
    this.show.next({
      event: event,
      folder: folder
    });
  }
}
