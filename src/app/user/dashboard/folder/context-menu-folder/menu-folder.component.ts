import {Component, OnInit} from '@angular/core';
import {MenuFolderService} from "./menu-folder.service";
import {FolderFormService} from "../folder-form/folder-form.service";
import {Folder} from "../folder";
import {FoldersService} from "../folders.service";
import {SelectedFolderService} from "../../selected-folder.service";

@Component({
  selector: 'folder-context-menu',
  templateUrl: './menu-folder.component.html',
  styleUrls: ['./menu-folder.component.css'],
  host:{
    '(document:click)':'clickedOutside()',
    '(window:scroll)':'clickedOutside()',
    '(window:resize)':'clickedOutside()'
  }
})
export class MenuFolderComponent implements OnInit {

  folder : Folder = null;
  isShown = false;

  private mouseLocation :{left:number,top:number} = {left:0,top:0};

  constructor(
    private contextMenuService: MenuFolderService,
    private folderService : FoldersService,
    private folderFormService : FolderFormService,
    private selectFodler : SelectedFolderService){
    this.contextMenuService.show.subscribe(e => {
      this.showMenu(e.event,e.folder);
    });
  }

  get locationCss(){
    let properties : any = {
      'z-index':10,
      'position':'fixed',
      'display':this.isShown ?  'block':'none'
    }

    let distanceRight = window.screen.width - this.mouseLocation.left;
    let distanceBottom = window.screen.height - this.mouseLocation.top;

    if(distanceRight > 400){
      properties.left = this.mouseLocation.left + 'px';
    }else{
      properties.left = this.mouseLocation.left - 200 + 'px';
    }

    if(distanceBottom > 500){
      properties.top = this.mouseLocation.top + 'px';
    }else{
      properties.top = this.mouseLocation.top + 'px';
    }

    return properties;
  }

  clickedOutside(){
    this.isShown= false
  }

  showMenu(event,folder){
    this.isShown = true;
    this.folder = folder;
    this.mouseLocation = {
      left:event.clientX,
      top:event.clientY
    }
  }

  deleteApp() {
    this.folderService.deleteFolder(this.folder.$key);
  }

  updateApp() {
    this.folderFormService.open(this.folder);
  }

  ngOnInit(){
  }

}
