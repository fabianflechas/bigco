import {Directive, HostListener, Input} from "@angular/core";
import {MenuFolderService} from "./menu-folder.service";
import {Folder} from "../folder";

@Directive({
  selector:'[folder-menu]'
})

export class MenuFolderDirective{
  @Input('folder-menu') folder : Folder;
  @HostListener('contextmenu', ['$event']) rightClicked(event:MouseEvent){
    console.log("en el directive, abriendo");
    event.preventDefault();
    if(this.folder.$key !== "myCoffer"){
      this._contextMenuService.showContext(event, this.folder);
    }
  }

  constructor(private _contextMenuService: MenuFolderService){
  }

}
