import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import {AuthService} from "../../core/services/auth.service";
import {Observable} from "rxjs/Observable";
import {Profile} from "../profile";

@Injectable()
export class ProfileService {

  constructor(private db : AngularFireDatabase, private authService: AuthService) { }

  getUserProfile() : Observable<Profile>{
    return this.db.object("/Users/"+this.authService.getCurrentUserId())
      .map(user =>
        Profile.fromJson(
          this.authService.getCurrentUserId(),
          user.publicData.name,
          user.publicData.surname,
          user.publicData.photoUrl,
          user.publicData.timestamp,
          user.publicData.userName,
          user.profile.birthday,
          user.profile.email,
          user.profile.sex
        )
      )
  }

  saveProfile(profile : Profile) {
    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/publicData/name").set(profile.name);
    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/publicData/surname").set(profile.surname);
    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/publicData/userName").set(profile.userName);
    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/publicData/photoUrl").set(profile.photoUrl);

    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/profile/birthday").set(profile.birthday);
    this.db.object("/Users/"+this.authService.getCurrentUserId()+"/profile/sex").set(profile.sex);
    return this.db.object("/Users/"+this.authService.getCurrentUserId()+"/profile/email").set(profile.email);
  }
}
