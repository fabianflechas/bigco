import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../profile.service";
import {Profile} from "../../profile";

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {

  profile : Profile;
  loading : boolean = false;
  successMsj : boolean = false;
  errMsj : boolean = false;

  constructor(private profileService : ProfileService) { }

  ngOnInit() {
    this.profileService.getUserProfile().subscribe(
      profile => this.profile = profile
    )
  }

  saveProfile() {
    this.loading = true;
    this.profileService.saveProfile(this.profile)
      .then(
        sccs => {
          this.loading = false;
          this.successMsj = true;
          setTimeout(()=>{this.successMsj = false}, 4000);
        })
      .catch(
        err => {
          this.loading = false;
          this.errMsj = true;
          setTimeout(()=>{this.errMsj = false}, 4000);
        })
  }

}
