import { Component, OnInit } from '@angular/core';
import {ProfileService} from "./profile.service";
import {Profile} from "../profile";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile : Profile;

  constructor(private profileService : ProfileService) { }

  ngOnInit() {
    this.profileService.getUserProfile().subscribe(
      profile => this.profile = profile
    )
  }

}
