import { NgModule } from '@angular/core';
import {ApplicationComponent} from "./dashboard/application/application.component";
import {MenuFolderComponent} from "./dashboard/folder/context-menu-folder/menu-folder.component";
import {MenuAppComponent} from "./dashboard/application/context-menu-app/menu-app.component";
import {GSearchComponent} from "./dashboard/g-search/g-search.component";
import {MenuAppDirective} from "./dashboard/application/context-menu-app/menu-app.directive";
import {MenuFolderDirective} from "./dashboard/folder/context-menu-folder/menu-folder.directive";
import {ApplicationFormComponent} from "./dashboard/application/application-form/application-form.component";
import {FolderComponent} from "./dashboard/folder/folder.component";
import {FolderFormComponent} from "./dashboard/folder/folder-form/folder-form.component";
import {IconPickerComponent} from "./dashboard/application/icon-picker/icon-picker.component";
import {IconFilterPipe} from "./dashboard/application/icon-picker/icon-filter.pipe";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {FoldersService} from "./dashboard/folder/folders.service";
import {ApplicationsService} from "./dashboard/application/applications.service";
import {SelectedFolderService} from "./dashboard/selected-folder.service";
import {MenuAppService} from "./dashboard/application/context-menu-app/menu-app.service";
import {MenuFolderService} from "./dashboard/folder/context-menu-folder/menu-folder.service";
import {SearchComponent} from "./menu-bar/search/search.component";
import {MenuBarComponent} from "./menu-bar/menu-bar.component";
import {SearchService} from "./menu-bar/search/search.service";
import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import {ProfileService} from "./profile/profile.service";
import { DashboardComponent } from './dashboard/dashboard.component';
import {ApplicationFormService} from "./dashboard/application/application-form/application-form.service";
import {FolderFormService} from "./dashboard/folder/folder-form/folder-form.service";
import { ProfileInfoComponent } from './profile/profile-info/profile-info.component';
import { ProfilePublicationsComponent } from './profile/profile-publications/profile-publications.component';
import { AddContentMenuComponent } from './content/add-content-menu/add-content-menu.component';
import { ContentComponent } from './content/content.component';
import { DiscoverComponent } from './discover/discover.component';
import { CreateContentFormComponent } from './content/create-content-form/create-content-form.component';
import { PublicationComponent } from './discover/publication/publication.component';
import {PictureComponent} from './content/forms/picture/picture.component';
import {CreateFormService} from './content/create-content-form/create-form.service';
import {ContentService} from './content/content.service';
import {PublicationService} from './discover/publication/publication.service'; //reveer y devatir donde dejamos el service


import {ImageCropperComponent} from 'ng2-img-cropper';//image cropper
import { Ng2FileDropModule }  from 'ng2-file-drop';
import { ContentPipe } from './discover/publication/content.pipe'; //image drop

const HOME_ROUTES = [
  {
    path: "",
    children: [
      {
        path:"",
        component: UserComponent,
        children: [
          {
            path:"",
            component: DashboardComponent
          },{
            path:"discover",
            component: DiscoverComponent
          },
          {
            path:"profile",
            component: ProfileComponent,
            children: [
              {
                path: "info",
                component: ProfileInfoComponent
              },
              {
                path: "wall",
                component: ProfilePublicationsComponent
              },
              {
                path: "",
                pathMatch: "full",
                redirectTo: "wall"
              }
            ]
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(HOME_ROUTES),
    Ng2FileDropModule
  ],
  declarations: [
    ApplicationComponent,
    MenuAppComponent,
    MenuFolderComponent,
    GSearchComponent,
    MenuAppDirective,
    MenuFolderDirective,
    ApplicationFormComponent,
    FolderComponent,
    FolderFormComponent,
    IconPickerComponent,
    IconFilterPipe,
    SearchComponent,
    MenuBarComponent,
    UserComponent,
    ProfileComponent,
    DashboardComponent,
    ProfileInfoComponent,
    ProfilePublicationsComponent,
    AddContentMenuComponent,
    ContentComponent,
    PictureComponent,
    DiscoverComponent,
    CreateContentFormComponent,
    PublicationComponent, 
    ImageCropperComponent, ContentPipe
  ],
  providers: [
    ApplicationsService,
    ApplicationFormService,
    FoldersService,
    FolderFormService,
    SelectedFolderService,
    MenuAppService,
    MenuFolderService,
    SearchService,
    ProfileService,
    CreateFormService,
    ContentService,
    PublicationService
  ]
})
export class UserModule { }
