import { Component, OnInit } from '@angular/core';
import {PublicationService} from './publication/publication.service';

 

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {

  private publications; 

  constructor(publicationService:PublicationService) {   
    publicationService.getPublications()
      .subscribe(publications =>{
        this.publications = publications;
      }
    );
  }

  ngOnInit() {
  }

}
