import { Injectable} from '@angular/core';

//services
import {ContentService} from '../../content/content.service'
import {AuthService} from "../../../core/services/auth.service";

//entities
//import {Cite} from '../../content/entities/cite';
import {Image} from '../../content/entities/image';
//import {Link} from '../../content/entities/link';
//import {Text} from '../../content/entities/text';
import {YoutubeVideo} from '../../content/entities/youtube-video';
import {Content} from '../../content/entities/content';


import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

declare var $;
 

@Injectable()
export class PublicationService {

  private pages:BehaviorSubject<number> = new BehaviorSubject<number>(1); 
  private PublicationslimitList=3;

  constructor(private db : AngularFireDatabase,
              private authService: AuthService,
              private contentService: ContentService) {}


  public chargeOnScroll(){
     $(window)
     .scroll(
      function(){
        if ($(window).scrollTop() == $(document).height()-$(window).height()){
          this.pages.next(this.pages.getValue() + 1);
          console.log('llegue :)', this.pages.getValue());
        }
      }
    );

  }


  public createPublication(contentKey: string ,anonimus :boolean){

    let publication = {
      timestamp : new Date().getTime(),
      anonimus: anonimus,
      user: this.authService.getCurrentUserId(),
      content: contentKey,
      comments: 0,
      likes: 0
    };
    return this.db.list("Publications").push(publication);
  }
 

  public getPublications():Observable<any>{
    return this.db.list('Publications',{
      query:{
        limitToFirst: (this.pages.getValue() * this.PublicationslimitList)
      } 
    });
  }


}
