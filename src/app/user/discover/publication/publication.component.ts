import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
  @Input('publication') publication;

  constructor() {
  }

  ngOnInit() {
  }

}
