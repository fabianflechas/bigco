import { Pipe, PipeTransform } from '@angular/core';
import {ContentService} from "../../content/content.service";

@Pipe({
  name: 'content'
})
export class ContentPipe implements PipeTransform {

  constructor(private contentService : ContentService){}

  transform(content: any, user: any): any {
    return this.contentService.getContent(user, content)
  }

}
