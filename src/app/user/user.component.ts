import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  template: `
    <app-menu-bar></app-menu-bar>
    <div class="user-content">
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
