import { TheLauncherOnePage } from './app.po';

describe('the-launcher-one App', () => {
  let page: TheLauncherOnePage;

  beforeEach(() => {
    page = new TheLauncherOnePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
